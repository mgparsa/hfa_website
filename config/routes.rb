Rails.application.routes.draw do
  get 'analysis/new'

  get 'sessions/new'

  get 'users/new'

  root 'main_pages#home'

  get 'main_pages/aboutus'
  
  get 'main_pages/home'
  
  get 'laststate' => 'analyses#laststate'
  
  get 'mainpanel' => 'mainpanel#index'

  get 'newanalysis' => 'analyses#new'
  
  get 'signup'  => 'users#new'
  
  get '/main_pages/contactus'
  
  get 'recommendation' => 'recommendation#index'
  get 'rec_nutrition' => 'recommendation#nutrition'
  get 'rec_exercise' => 'recommendation#exercise'
  get 'rec_dailyTips' => 'recommendation#dailyTips'
  get 'rec_mobileapp' => 'recommendation#mobileapp'
  resources :users
  
  resources :analyses
  resources :analysis_steps
  
  get    'login'   => 'sessions#new'
  post   'login'   => 'sessions#create'
  delete 'logout'  => 'sessions#destroy'
end
