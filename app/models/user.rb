class User < ActiveRecord::Base
    before_save { self.username = username.downcase }
    validates :username,  presence: true, length: { maximum: 50 }, uniqueness: {case_sensitive: false}
    VALID_AGE_REGEX = /\d/
    validates :age, presence: true, length: { maximum: 3 },format: { with: VALID_AGE_REGEX}
    
    validates :sex,  presence: true
    
    has_secure_password
    validates :password, presence: true, length: { minimum: 6 }
end
