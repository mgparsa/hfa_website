class AnalysesController < ApplicationController
  before_action :set_analysis, only: [:show, :edit, :update, :destroy]

  # GET /analyses
  # GET /analyses.json
  def index
    @analyses = Analysis.all
  end

  # GET /analyses/1
  # GET /analyses/1.json
  def show
   
  end
  
  def laststate
   @analysis = Analysis.find(1)
  end

  # GET /analyses/new
  def new
    
    @analysis = Analysis.new
    
  end

  def edit
  end
def create
  oldexist = Analysis.find_by(userid: current_user.id)
  if oldexist == nil
    @analysis = Analysis.new(analysis_params)
    @analysis.userid = current_user.id
    @analysis.avalue = calculate_a_value
    if @analysis.save
      session[:analysis_id] = @analysis.id
      redirect_to analysis_steps_path
    else
      render :new
    end
  else
    @analysis = oldexist
    @analysis.update_attributes(analysis_params)
    @analysis.userid = current_user.id
    @analysis.avalue = calculate_a_value
    @analysis.save
    session[:analysis_id] = @analysis.id
    redirect_to analysis_steps_path
  end
end
  # PATCH/PUT /analyses/1
  # PATCH/PUT /analyses/1.json
  def update
    respond_to do |format|
      if @analysis.update(analysis_params)
        format.html { redirect_to @analysis, notice: 'Analysis was successfully updated.' }
        format.json { render :show, status: :ok, location: @analysis }
      else
        format.html { render :edit }
        format.json { render json: @analysis.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /analyses/1
  # DELETE /analyses/1.json
  def destroy
    @analysis.destroy
    respond_to do |format|
      format.html { redirect_to analyses_url, notice: 'Analysis was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_analysis
      @analysis = Analysis.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def analysis_params
      params.require(:analysis).permit(:userid, :weight, :length, :waist, :hip, :avalue, :push_up, :sit_up, :bvalue, :phalanx_number, :cvalue, :fitness)
    end
    
    def calculate_a_value
      alfa = @analysis.waist.fdiv(@analysis.hip)
      beta = @analysis.weight.fdiv(@analysis.length * @analysis.length )
      if current_user.sex == "female" and alfa > 0.95
        return 1
      end
      if current_user.sex == "male" and alfa > 0.8
        return 1
      end
      if beta >= 40
        return 2
      elsif beta >=30 and beta <40
        return 3
      elsif beta >=25 and beta <30
        return 4
      elsif beta >=18.5 and beta <25
        return 5
      end
      return -1
    end
end
