class AnalysisStepsController < ApplicationController
  include Wicked::Wizard
  steps :muscular, :stretch
  
  def show
    @analysis = Analysis.find(session[:analysis_id])
    @analysis.bvalue = calculate_b_value
    @analysis.save
    render_wizard
  end
  
  def update
    @analysis = Analysis.find(session[:analysis_id])
    @analysis.attributes = analysis_params
    @analysis.cvalue = @analysis.phalanx_number
    @analysis.fitness = calculate_fitness
    @analysis.save
    render_wizard @analysis
  end
  
  private
  def analysis_params
      params.require(:analysis).permit(:userid, :weight, :length, :waist, :hip, :avalue, :push_up, :sit_up, :bvalue, :phalanx_number, :cvalue, :fitness)
  end
  
    def calculate_b_value
      gama = 0
      teta = 0
      if @analysis.sit_up >=0 and @analysis.sit_up <10
        gama =1;
      elsif @analysis.sit_up >=10 and @analysis.sit_up <20
        gama =2;
      elsif @analysis.sit_up >=20 and @analysis.sit_up <30
        gama =3;
      elsif @analysis.sit_up >=30
        gama =4;
      end
      if @analysis.push_up >=0 and @analysis.push_up <10
        teta =1;
      elsif @analysis.push_up >=10 and @analysis.push_up <20
        teta =2;
      elsif @analysis.push_up>=20 and @analysis.push_up <30
        teta =3;
      elsif @analysis.push_up>=30
        teta =4;
      end
      res = gama+teta
      if res <=2
        return 1
      end
      if res>2 and  res <=6
        return 2
      end
      if res>=8 and  res <=9
        return 3
      end
      if res>=12
        return 4
      end
      return -1
    end
    def calculate_fitness
      f = 8*@analysis.avalue + 8*@analysis.bvalue + 7*@analysis.cvalue
      return f
    end
end