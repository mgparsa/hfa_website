json.array!(@analyses) do |analysis|
  json.extract! analysis, :id, :userid, :weight, :length, :waist, :hip, :avalue, :push_up, :sit_up, :bvalue, :phalanx_number, :cvalue, :fitness
  json.url analysis_url(analysis, format: :json)
end
