class CreateAnalyses < ActiveRecord::Migration
  def change
    create_table :analyses do |t|
      t.integer :userid
      t.integer :weight
      t.integer :length
      t.integer :waist
      t.integer :hip
      t.integer :avalue
      t.integer :push_up
      t.integer :sit_up
      t.integer :bvalue
      t.integer :phalanx_number
      t.integer :cvalue
      t.integer :fitness

      t.timestamps null: false
    end
  end
end
