class ChangeDateFormatInAnalyses < ActiveRecord::Migration
  def change
      change_column :analyses, :weight, :float
      change_column :analyses, :length, :float
      change_column :analyses, :waist, :float
      change_column :analyses, :hip, :float
      change_column :analyses, :avalue, :float
      change_column :analyses, :bvalue, :float
      change_column :analyses, :cvalue, :float
      change_column :analyses, :fitness, :float
  end
end
